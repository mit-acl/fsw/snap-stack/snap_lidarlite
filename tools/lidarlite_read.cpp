/**
 * @file lidarlite_read.cpp
 * @brief CPU entry point for querying power measurement board
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 August 2020
 */

#include <iostream>

#include "snap_lidarlite/snap_lidarlite.h"

int main(int argc, char const *argv[])
{
  acl::SnapLidarLite lidar;

  const double distance = lidar.distance();
  std::cout << "lidarlite  distance (m): " << distance << std::endl;

  return 0;
}
