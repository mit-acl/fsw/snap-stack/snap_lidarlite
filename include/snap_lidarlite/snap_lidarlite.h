/**
 * @file snap_lidarlite.h
 * @brief LIDAR Lite API for Snapdragon
 * @author Parker Lusk <plusk@mit.edu>
 * @date 9 August 2020
 */

#pragma once

#include <cstdint>

namespace acl {

class SnapLidarLite
{
public:
    SnapLidarLite();
    ~SnapLidarLite();

    double distance();
    bool hw_error() const { return hw_error_; }

private:
    bool hw_error_ = false; ///< cannot communicate with device
};

} // ns acl
