Snapdragon Lidar Lite v3 Interface
==================================

## Building

1. Clone into the [`sfpro-dev`](https://gitlab.com/mit-acl/fsw/snap-stack/sfpro-dev) or [`sf-dev`](https://gitlab.com/mit-acl/fsw/snap-stack/sf-dev) `workspace` directory.
2. Update submodules: `git submodule update --init --recursive`.
3. Follow appropriate instructions for building and pushing via `adb`: `./build.sh workspace/snap_pmb --load`.

## Implementation Notes

- Based on Garmin's [Arduino LIDAR-Lite library](https://github.com/garmin/LIDARLite_Arduino_Library).
- Must be attached to I2C bus on J1 of sfpro (only 5v I2C bus available)

## Example Output

### lidarlite_read

Run `./lidarlite_read` (in root dir) to read the current distance from sensor:

### mini-dm

The following example output was obtained via `mini-dm` (i.e., `make mini-dm` from [sf-dev](https://gitlab.com/mit-acl/fsw/snap-stack/sf-dev) or [sfpro-dev](https://gitlab.com/mit-acl/fsw/snap-stack/sfpro-dev)) on an Excelsior 8096 (sfpro).

```bash
[08500/02]  03:48.068  device open entered, device path: /dev/iic-8  0054  dev_fs.c
[08500/02]  03:48.068  Looking for bus id 8  0067  i2c.c
[08500/02]  03:48.068  initialize the i2c DAL driver: 33554631  0086  i2c.c
[08500/02]  03:48.072  [lidarlitev3] Successfully opened '/dev/iic-8' and configured for slave 0x62.  0053  lidarlitev3.c
[08500/02]  03:48.072  [lidarlite] Successfully initialized.  0030  lidarlite_imp.c
[08500/02]  03:48.075  [lidarlite] Distance (m): 0.00	t: 12691435999642127576  0051  lidarlite_imp.c
[08500/02]  03:48.075  [lidarlitev3] Deinitialized.  0070  lidarlitev3.c
[08500/02]  03:48.075  [lidarlite] Closed.  0062  lidarlite_imp.c
```
