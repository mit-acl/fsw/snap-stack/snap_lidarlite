/**
 * @file lidarlite_imp.c
 * @brief DSP implementation to interface with LIDAR-Lite via I2c
 * @author Parker Lusk <plusk@mit.edu>
 * @date 9 August 2020
 */

#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <time.h>
#include <dspal_time.h>
#include <sys/ioctl.h>
#include <dev_fs_lib_i2c.h>

#include "utils.h"
#include "lidarlitev3.h"

static void i2c_sniff(int i2cdevnum);

// ----------------------------------------------------------------------------

int lidarlite_init()
{
  // Open /dev/iic-# (note: using BLSP, not SSC).
  const uint8_t i2c_devnum = 8; // This is J1 on APQ8096
  bool success = lidarlitev3_begin(i2c_devnum, LIDARLITE_DEFAULT_ADDR, 0);
  if (!success) return LIDARLITE_ERROR;

  LOG_INFO("[lidarlite] Successfully initialized.");

  // i2c_sniff(8);

  return LIDARLITE_SUCCESS;
}

// ----------------------------------------------------------------------------

int lidarlite_distance(float * dist, uint64_t * time_us)
{
  struct timespec tp;
  clock_gettime(CLOCK_REALTIME, &tp);
  *time_us = tp.tv_sec * 1e6 + tp.tv_nsec * 1e-3;

  // distance in centimeters
  uint32_t dist_cm = lidarlitev3_distance(true);

  // distance in meters
  *dist = ((float)dist_cm) * 1e-2;

  LOG_INFO("[lidarlite] Distance (m): %.2f\tt: %llu", *dist, time_us);

  return LIDARLITE_SUCCESS;
}

// ----------------------------------------------------------------------------

void lidarlite_close(void)
{
  lidarlitev3_close();

  LOG_INFO("[lidarlite] Closed.");
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

static void i2c_sniff(int i2cdevnum)
{

  // open I2C device
  char dev[256];
  snprintf(dev, sizeof(dev), DEV_FS_I2C_DEVICE_TYPE_STRING "%d", i2cdevnum);
  int fd = open(dev, O_RDWR);

  for (uint8_t i=0; i<127; ++i) 
  {
    // Configure I2C with slave address
    struct dspal_i2c_ioctl_slave_config slave_config;
    slave_config.slave_address = i;
    slave_config.bus_frequency_in_khz = 400;
    slave_config.byte_transer_timeout_in_usecs = 9000;
    ioctl(fd, I2C_IOCTL_CONFIG, &slave_config);

    // query for device on this bus with addr i
    uint8_t data = 0;
    uint8_t len = write(fd, &data, 1);
    if (len == 1) {
      LOG_INFO("0x%x: Device found. Sent %d bytes", i, len);
    }

    usleep(100000);
  }

  // close I2C device
  close(fd);

}
