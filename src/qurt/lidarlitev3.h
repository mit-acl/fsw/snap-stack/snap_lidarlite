/**
 * @file lidarlitev3.h
 * @brief C API for LIDAR Lite v3 (based on Arduino library)
 * @author Parker Lusk <plusk@mit.edu>
 * @date 9 August 2020
 */

#pragma once

#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>

#define LIDARLITE_DEFAULT_ADDR 0x62

bool lidarlitev3_begin(uint8_t i2cdevnum, uint8_t addr, uint8_t configuration);
void lidarlitev3_close();
void lidarlitev3_reset();
uint32_t lidarlitev3_distance(bool biasCorrection);
