/**
 * @file lidarlitev3.c
 * @brief C API for LIDAR Lite v3 (based on Arduino library)
 * @author Parker Lusk <plusk@mit.edu>
 * @date 9 August 2020
 */

#include <fcntl.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <dev_fs_lib_i2c.h>

#include "utils.h"
#include "lidarlitev3.h"

static int fd_ = -1;
static uint8_t addr_ = 0x00;

static bool _i2c_slave_config(uint8_t addr);
static void _configure(uint8_t configuration);
static bool _write(uint8_t reg, uint8_t val);
static bool _read16(uint8_t reg, uint8_t buf[2]);

// ----------------------------------------------------------------------------

bool lidarlitev3_begin(uint8_t i2cdevnum, uint8_t addr, uint8_t configuration)
{
  // 
  // Open the requested I2C device
  //

  char dev[256];
  snprintf(dev, sizeof(dev), DEV_FS_I2C_DEVICE_TYPE_STRING "%d", i2cdevnum);
  fd_ = open(dev, O_RDWR);

  if (fd_ == -1) {
    LOG_ERR("[lidarlitev3] Opening '%s' failed.", dev);
    return false;
  }

  //
  // Configure the I2C device and sensor
  //

  addr_ = addr;

  bool ret = _i2c_slave_config(addr);
  if (!ret) return false;

  _configure(configuration);

  LOG_INFO("[lidarlitev3] Successfully opened '%s' "
           "and configured for slave 0x%x.", dev, addr);

  return true;
}

// ----------------------------------------------------------------------------

void lidarlitev3_close()
{
  if (fd_ == -1) return;

  // close the file descriptor
  close(fd_);

  // indicate that device is closed
  fd_ = -1;

  LOG_INFO("[lidarlitev3] Deinitialized.");
}

/*------------------------------------------------------------------------------
  Reset

  Reset device. The device reloads default register settings, including the
  default I2C address. Re-initialization takes approximately 22ms.
------------------------------------------------------------------------------*/
void lidarlitev3_reset()
{
  _write(0x00, 0x00);
}

/*------------------------------------------------------------------------------
  Distance

  Take a distance measurement and read the result.

  Process
  ------------------------------------------------------------------------------
  1.  Write 0x04 or 0x03 to register 0x00 to initiate an aquisition.
  2.  Read register 0x01 (this is handled in the read() command)
      - if the first bit is "1" then the sensor is busy, loop until the first
        bit is "0"
      - if the first bit is "0" then the sensor is ready
  3.  Read two bytes from register 0x8f and save
  4.  Shift the first value from 0x8f << 8 and add to second value from 0x8f.
      The result is the measured distance in centimeters.

  Parameters
  ------------------------------------------------------------------------------
  biasCorrection: Default true. Take aquisition with receiver bias
    correction. If set to false measurements will be faster. Receiver bias
    correction must be performed periodically. (e.g. 1 out of every 100
    readings).
------------------------------------------------------------------------------*/
uint32_t lidarlitev3_distance(bool biasCorrection)
{
  if (biasCorrection) {
    // Take acquisition & correlation processing with receiver bias correction
    _write(0x00, 0x04);
  } else {
    // Take acquisition & correlation processing without receiver bias correction
    _write(0x00, 0x03);
  }
  // Array to store high and low bytes of distance
  uint8_t distanceArray[2] = { 0 };
  // Read two bytes from register 0x8f (autoincrement for reading 0x0f and 0x10)
  _read16(0x8f, distanceArray);
  // Shift high byte and add to low byte
  uint32_t distance = (distanceArray[0] << 8) + distanceArray[1];
  return distance;
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

static bool _i2c_slave_config(uint8_t addr)
{
  struct dspal_i2c_ioctl_slave_config slave_config;
  slave_config.slave_address = addr;
  slave_config.bus_frequency_in_khz = 400;
  slave_config.byte_transer_timeout_in_usecs = 9000;

  bool ret = ioctl(fd_, I2C_IOCTL_CONFIG, &slave_config);

  if (ret != 0) {
    LOG_ERR("[lidarlitev3] IOCTL slave 0x%x failed.", addr);
    return false;
  }

  return true;
}

/*------------------------------------------------------------------------------
  Configure

  Selects one of several preset configurations.

  Parameters
  ------------------------------------------------------------------------------
  configuration:  Default 0.
    0: Default mode, balanced performance.
    1: Short range, high speed. Uses 0x1d maximum acquisition count.
    2: Default range, higher speed short range. Turns on quick termination
        detection for faster measurements at short range (with decreased
        accuracy)
    3: Maximum range. Uses 0xff maximum acquisition count.
    4: High sensitivity detection. Overrides default valid measurement detection
        algorithm, and uses a threshold value for high sensitivity and noise.
    5: Low sensitivity detection. Overrides default valid measurement detection
        algorithm, and uses a threshold value for low sensitivity and noise.
------------------------------------------------------------------------------*/
static void _configure(uint8_t configuration)
{
  switch (configuration)
  {
    case 0: // Default mode, balanced performance
      _write(0x02, 0x80); // Default
      _write(0x04, 0x08); // Default
      _write(0x1c, 0x00); // Default
    break;

    case 1: // Short range, high speed
      _write(0x02, 0x1d);
      _write(0x04, 0x08); // Default
      _write(0x1c, 0x00); // Default
    break;

    case 2: // Default range, higher speed short range
      _write(0x02, 0x80); // Default
      _write(0x04, 0x00);
      _write(0x1c, 0x00); // Default
    break;

    case 3: // Maximum range
      _write(0x02, 0xff);
      _write(0x04, 0x08); // Default
      _write(0x1c, 0x00); // Default
    break;

    case 4: // High sensitivity detection, high erroneous measurements
      _write(0x02, 0x80); // Default
      _write(0x04, 0x08); // Default
      _write(0x1c, 0x80);
    break;

    case 5: // Low sensitivity detection, low erroneous measurements
      _write(0x02, 0x80); // Default
      _write(0x04, 0x08); // Default
      _write(0x1c, 0xb0);
    break;
  }
}

/*------------------------------------------------------------------------------
  Write

  Perform I2C write to device.

  Parameters
  ------------------------------------------------------------------------------
  reg: register address to write to.
  val: value to write.
------------------------------------------------------------------------------*/
static bool _write(uint8_t reg, uint8_t val)
{
  const uint8_t len = 2;
  uint8_t data[len] = { reg, val };
  uint8_t byte_count = write(fd_, data, len);

  if (byte_count != len) {
    LOG_ERR("[lidarlitev3] Expected %d bytes written, sent %d", len, byte_count);
    return false;
  }

  return true;
}

/*------------------------------------------------------------------------------
  Read

  Perform I2C read from device. Will detect an unresponsive device and return
  a false.

  Busy flag monitoring can is used to read registers that are updated at the
  end of a distance measurement to obtain the new data.

  Parameters
  ------------------------------------------------------------------------------
  reg: register address to read from.
  buf: an array to store the read values (len 2).
------------------------------------------------------------------------------*/
static bool _read16(uint8_t reg, uint8_t buf[2])
{
  bool busyFlag = true;
  uint16_t busyCounter = 0;

  // wait until status register says the device is not busy
  while (busyFlag) {
    uint8_t statusReg = 0x01; // the status register to be read
    uint8_t status = 0xff;
    write(fd_, &statusReg, 1);
    read(fd_, &status, 1);

    // busy flag is LSB of status reg
    busyFlag = ((status & 0x01) == 0x01);

    // if we have been busy for too long, bail
    if (busyCounter++ > 999) {
      LOG_ERR("[lidarlitev3] _read16 too busy");
      return false;
    }
  }

  LOG_DEBUG("[lidarlitev3] ready to read data");

  // device is not busy, begin read
  uint8_t byte_count = write(fd_, &reg, 1);
  read(fd_, buf, 2);

  return true;
}
