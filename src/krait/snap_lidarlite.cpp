/**
 * @file snap_lidarlite.cpp
 * @brief LIDAR Lite API for Snapdragon
 * @author Parker Lusk <plusk@mit.edu>
 * @date 9 August 2020
 */

#include "snap_lidarlite/snap_lidarlite.h"

#include "utils.h"
#include "lidarlite.h"

namespace acl {

SnapLidarLite::SnapLidarLite()
{
    // Make sure we can connect to the device
    if (lidarlite_init() != LIDARLITE_SUCCESS) {
        LOG_ERR("Hardware error: cannot initialize DSP-side peripheral.");
        hw_error_ = true;
    }
}

// ----------------------------------------------------------------------------

SnapLidarLite::~SnapLidarLite()
{
    if (!hw_error_) lidarlite_close();
}

// ----------------------------------------------------------------------------

double SnapLidarLite::distance()
{
    uint64_t time_us;
    float distance;
    lidarlite_distance(&distance, &time_us);
    return static_cast<double>(distance);
}

} // ns acl
